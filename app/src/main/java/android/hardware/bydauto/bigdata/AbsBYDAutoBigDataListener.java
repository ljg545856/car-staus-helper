package android.hardware.bydauto.bigdata;

public abstract class AbsBYDAutoBigDataListener {
    public void onNeedRendRegisterTable(int value) {
    }

    public void onWholeFrameDataChanged(byte[] data) {
    }
}
